<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\driver\mongo
{
	use nuclio\core\ClassManager;
	use \MongoClient;
	use \MongoDB;
	use \MongoCollection;
	use \MongoCursor;
	use \MongoId;
	use \MongoDate;
	use \MongoCode;
	use \MongoException;
	use nuclio\plugin\database\
	{
		exception\DatabaseException,
		common\CommonInterface,
		common\CommonCursorInterface,
		common\DBRecord,
		common\DBVector,
		common\DBQuery,
		common\DBFields,
		common\DBOptions,
		common\DBCondition,
		orm\Model
	};
	use nuclio\plugin\
	{
		provider\manager\Manager as ProviderManager,
		versionControl\VersionControl
	};
	use nuclio\helper\CollectionHelper;
	
	<<provides('database::mongo')>>
	class Mongo implements CommonInterface
	{
		const string ASCENDING					=MongoCollection::ASCENDING;
		const string DESCENDING					=MongoCollection::DESCENDING;
		const string HANDLER_MONGO				='mongo';
		const string OPT_TYPE					='type';
		const string OPT_HOST					='host';
		const string OPT_PORT					='port';
		const string OPT_DATABASE				='database';
		const string OPT_USERNAME				='username';
		const string OPT_PASSWORD				='password';
		const string OPT_REPLICA_SET			='replicaSet';
		const string MONGO_CONNECTION_USERNAME	='username';
		const string MONGO_CONNECTION_PASSWORD	='password';
		const string MONGO_CONNECTION_DATABASE	='database';
		const string DEFAULT_HOST				='localhost';
		const int DEFAULT_PORT					=27017;
		const string LOOPBACK_IP				='127.0.0.1';
		const string DEFAULT_PASSWORD			='';
		const string MONGO_ID					='_id';
		const string NORMAL_ID					='id';
		const string MONGO_DATE					='datetime';
		const string VERSION_COLLECTION			='version';
		
		protected string $database				='';
		protected string $collection			='';

		/**
		 * @var \MongoClient|null
		 */
		protected MongoClient $connection;
		
		protected MongoDB $databasePointer;

		public static function getInstance(/* HH_FIXME[4033] */...$args):Mongo
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public function __construct(Map<string,string> $options=Map{})
		{
			if(!$options->containsKey(self::OPT_HOST))
			{
				if(is_string(self::OPT_HOST))
				{
					$options[self::OPT_HOST] = self::LOOPBACK_IP;
				}
			} 
			else if($options[self::OPT_HOST] == self::DEFAULT_HOST) 
			{
				$options[self::OPT_HOST] = self::LOOPBACK_IP;
			}
			if($options->containsKey(self::OPT_USERNAME) && !$options->containsKey(self::OPT_PASSWORD))
			{
				$options[self::OPT_PASSWORD] = self::DEFAULT_PASSWORD;
			}
			try
			{
				$connectionOptions = array();

				if ($options->containsKey(self::OPT_DATABASE))
				{
					$this->database = $options[self::OPT_DATABASE];
					$connectionOptions[self::MONGO_CONNECTION_DATABASE] = $this->database;
				}
				if ($options->containsKey(self::OPT_REPLICA_SET))
				{
					$connectionOptions[self::OPT_REPLICA_SET]=$options[self::OPT_REPLICA_SET];
				}
				
				// if($options->containsKey(self::OPT_USERNAME))
				// {
				// 	$connectionOptions[self::MONGO_CONNECTION_USERNAME] = $options[self::OPT_USERNAME];
				// 	$connectionOptions[self::MONGO_CONNECTION_PASSWORD] = $options[self::OPT_PASSWORD];
				// }
				$this->connection=new MongoClient
				(
					$this->createConnectionString($options),
					$connectionOptions
				);
				if (!is_null($this->database))
				{
					$this->databasePointer=$this->connection->selectDB($this->database);
				}
				else
				{
					throw new DatabaseException('Please specify a default database to connect to.');
				}
			}
			catch(MongoException $exception)
			{
				throw new DatabaseException($exception->getMessage());
			}
		 }

		/**
		 * Make Connetion String for Using Mongo
		 * @param  array $options define config of db
		 * @return String database connection string
		 */

		private function createConnectionString(Map<string,string> $options):string
		{
			$username	='';
			$password	='';
			$host		='';
			$port		=self::DEFAULT_PORT;
			$database	='';
			if ($options->containsKey(self::OPT_HOST))
			{
				$host=$options[self::OPT_HOST];
			}
			if ($options->containsKey(self::OPT_PORT))
			{
				$port=$options[self::OPT_PORT];
			}
			if ($options->containsKey(self::OPT_DATABASE))
			{
				$database=$options[self::OPT_DATABASE];
			}
			if ($options->containsKey(self::OPT_USERNAME))
			{
				$username=$options[self::OPT_USERNAME];
				if ($options->containsKey(self::OPT_USERNAME))
				{
					$password=$options[self::OPT_PASSWORD];
				}
				$dbstring=sprintf
				(
					'mongodb://%s:%s@%s:%s/%s',
					$username,
					$password,
					$host,
					$port,
					$database
				);
			}
			else
			{
				$dbstring=sprintf
				(
					'mongodb://%s:%s/%s',
					$host,
					$port,
					$database
				);
			}
			return $dbstring;
		}

		/**
		 * Changes the active database
		 * @param  string $database the name of the DB to load
		 * @return Mongo           the current object
		 */
		
		public function selectDb(string $database):MongoDB
		{
			return $this->databasePointer=$this->connection->selectDB($database);
		}

		/**
		 * Overloader method for dealing with a case where the PDO object is lost on a variable.
		 * 
		 * @access public
		 * @return void
		 */
		public function __destruct():void
		{
			$this->disconnect();
		}

		/**
		 * Disconnects the database connection.
		 *
		 * @access public
		 * @return $this
		 */
		public function disconnect():void
		{
			unset($this->connection);
			//return $this;
		}


		/**
		 * Fetches the collection object matching the passed name.
		 * The method will always return a valid collection object, even if the
		 * collection does not exist yet (implicit creation).
		 * @param  string $collectionName	the name of the collection to use
		 * @return MongoCollection			the collection object
		 */
		
		public function getCollection(string $collectionName):MongoCollection
		{
			return $this->databasePointer->selectCollection($collectionName);
		}

		/**
		* The object is modified and gains an _id key (if it wasn't already defined)
		* after a successful insert.
		 * @param  string $collection the name of the table
		 * @param  array $record the new object to insert
		 */

		public function upsert(string $target, DBRecord $record, DBQuery $condition=Map{}, DBOptions $options=Map{}):DBRecord
		{
			// $version=VersionControl::getInstance();
			// if($target!='version')
			// {
			// 	$version->getBefore($target, $record);
			// }
			try
			{
				$ops=[];
				// if ($record->containsKey(self::MONGO_ID))
				// {
				// 
					$record	=$this->setMongoIdRecursive($record)
							|>$this->setMongoDateRecursive($$);
					
					// $condition=$condition->toArray();
					
					//Convert the Map to an array.
					$recordArray=$record->toArray();
					
					//Save the record.
					$this->getCollection($target)->save($recordArray,$ops);
					
					//Fetch back the record and return it.
					$record=$this->getCollection($target)->findOne($recordArray);
					// $this->getCollection($target)->update
					// (
					// 	$condition->toArray(),
					// 	$record->toArray(),
					// 	$ops
					// );
				// }
				// else
				// {
				// 	$this->getCollection($target)->save($record->toArray(),$ops);
				// }
				
				// if($target!='version')
				// {
				// 	$version->checkDiff($target, $record);
				// }
				
				return $this->normalizeResult($record);
			}
			catch(MongoException $exception)
			{
				throw new DatabaseException($exception->getMessage());
			}
		}
		
		// **
		// * Getting Unique Records only with selected colum.
		//  * @param  string $collection the name of the table
		//  * @param  string $field the name of field
		//  * @param  array $query passing variable using array.
		//  */
	
		// public function distinct(string $target, array $query=array(), array $fields=array()):Map<string,mixed>
		// {
		// 	$resultSet		=array();
		// 	try
		// 	{
		// 		$cursor		=$this->getCollection($target)->distinct($fields,$query);
		// 		$resultSet	=$cursor;
		// 	}
		// 	catch(MongoException $exception)
		// 	{
		// 		throw new DatabaseException($exception->getMessage());
		// 	}
		// 	return new Map($resultSet);
		// }
		
		public function find(string $target, DBQuery $query, DBOptions $options=Map{}):CommonCursorInterface
		{
			$query		=$this->parseModelRefsInQuery($query);
			$query		=$this->setMongoIdRecursive($query);
			$fields		=[];
			if ($options->containsKey('fields'))
			{
				$fields=$options->get('fields');
			}
			$mongoCursor=$this->getCollection($target)->find($query->toArray(),$fields);
			$cursor		=ProviderManager::request('database::mongo::cursor',$mongoCursor);
			if ($cursor instanceof CommonCursorInterface)
			{
				return $cursor;
			}
			else
			{
				throw new Exception('Expected an instance of CommonCursorInterface but didn\'t receive one from the Provider Manager. Please check that you are using a compatible driver.');
			}
		}
		
		public function findOne(string $target, DBQuery $query, DBOptions $options=Map{}):?DBRecord
		{
			$query	=$this->parseModelRefsInQuery($query);
			$query	=$this->setMongoIdRecursive($query);
			$fields	=[];
			if ($options->containsKey('fields'))
			{
				$fields=$options->get('fields');
			}
			$result=$this->getCollection($target)->findOne($query->toArray(),$fields);
			if (!is_null($result))
			{
				return $this->normalizeResult($result);
			}
			return null;
		}
		
		public function findById(string $target, mixed $id, DBOptions $options=Map{}):?DBRecord
		{
			$id=$this->createMongoIDObject($id);
			if (is_null($id))
			{
				return null;
			}
			$fields=[];
			if ($options->containsKey('fields'))
			{
				$fields=$options->get('fields');
			}
			$result=$this->getCollection($target)->findOne
			(
				[self::MONGO_ID=>$id],
				$fields
			);
			if (!is_null($result))
			{
				return $this->normalizeResult($result);
			}
			return null;
		}
		
		public function distinct(string $target, string $field):Vector<mixed>
		{
			return new Vector($this->getCollection($target)->distinct($field));
		}
		
		public function listCollections():Vector<mixed>
		{
			$collections=$this->databasePointer->listCollections();
			return new Vector($collections);
		}
		
		public function listCollectionFields(string $collection):Vector<Pair<string,sting>>
		{
			$map=<<<JS
			function()
			{
				for (var key in this)
				{
					emit(key,null);
				}
			}
JS;
			$reduce=<<<JS
			function(key,stuff)
			{
				return null;
			}
JS;
			$this->mapReduce($collection,$map,$reduce,$collection.'_keys');
		}
		
		public function aggregate(string $collection, Vector<array<string,mixed>> $pipeline):?Map<array<string,mixed>>
		{
			// var_dump($pipeline->toArray());
			$result=new Map($this->getCollection($collection)->aggregate($pipeline->toArray()));//,['allowDiskUse'=>true]);
			if ($result->containsKey('result'))
			{
				return new Map($result->get('result'));
			}
			return null;
			// var_dump($result);
			// exit();
			// $cursor		=ProviderManager::request('database::mongo::cursor',$mongoCursor);
			// if ($cursor instanceof CommonCursorInterface)
			// {
			// 	return $cursor;
			// }
			// else
			// {
			// 	throw new Exception('Expected an instance of CommonCursorInterface but didn\'t receive one from the Provider Manager. Please check that you are using a compatible driver.');
			// }
		}
		
		public function mapReduce(string $collection, string $map, string $reduce, string $resultCollection):void
		{
			$result=$this->databasePointer->command
			(
				[
					'mapreduce'	=>$collection,
					// 'map'		=>new MongoCode($map),
					'reduce'	=>new MaongoCode($reduce),
					'out'		=>['merge'=>'_keys']
				]
			);
			// var_dump($result);
			exit('STOP');
		}
		
		private function parseModelRefsInQuery(DBQuery $query):DBQuery
		{
			$newQuery=Map{};
			foreach ($query as $key=>$val)
			{
				if ($val instanceof Model)
				{
					$newQuery[$key.'._orm._id']=$val->getId();
				}
				else
				{
					$newQuery[$key]=$val;
				}
			}
			return $newQuery;
		}
		
		private function setMongoIdRecursive(DBQuery $object):DBQuery
		{
			$val=null;
			$newObject=Map{};
			foreach ($object as $key=>$val)
			{
				$mongoOperators = Vector
				{
					'$in',
					'$nin'
				};

				if($mongoOperators->linearSearch($key)>=0)
				{
					if($val instanceof Vector)
					{
						$ids = Vector{};
						foreach($val as $value)
						{
							$ids->add($this->createMongoIDObject($value));
						}
						$newObject->set($key, $ids);
					}
				}
				else if (($key===self::MONGO_ID || strstr($key,'.'.self::MONGO_ID)) && !$val instanceof MongoId)
				{
					if($val instanceof Map)
					{
						$newObject->set($key,$this->setMongoIdRecursive($val));
					}
					else
					{
						$newObject->set($key,$this->createMongoIDObject($val));
					}
				}
				else if ($key===self::NORMAL_ID)
				{
					if (!$val instanceof MongoId)
					{
						$newObject->set(self::MONGO_ID,$this->createMongoIDObject($val));
					}
					else
					{
						$newObject->set(self::MONGO_ID,$val);
					}
				}
				else if (is_array($val))
				{
					$newObject->set($key,$this->setMongoIdRecursiveArray($val));
				}
				else if ($val instanceof Map)
				{
					$newObject->set($key,$this->setMongoIdRecursive($val));
				}
				else
				{
					$newObject->set($key,$val);
				}
			}
			return $newObject;
		}
		
		private function setMongoIdRecursiveArray(array<mixed> $object):array<mixed>
		{
			$val=null;
			// try
			// {
				foreach ($object as $key=>$val)
				{
					$key=(string)$key;
					if (($key===self::MONGO_ID || strstr($key,'.'.self::MONGO_ID)) && !$val instanceof MongoId)
					{
						if ($val==='')
						{
							// $val=null;
							continue;
						}
						$object[$key]=$this->createMongoIDObject($val);
					}
					else if (is_array($val))
					{
						$object[$key]=$this->setMongoIdRecursiveArray($val);
					}
				}
			// }
			// catch (MongoException $exception)
			// {
				// var_dump($exception);
				// exit();
				// $formattedVal=$val;
				// if (!is_string($formattedVal))
				// {
				// 	if (is_array($formattedVal))
				// 	{
				// 		$formattedVal=print_r($formattedVal,true);
				// 	}
				// 	else if ($formattedVal instanceof Map
				// 	|| $formattedVal instanceof Vector)
				// 	{
				// 		$formattedVal=(string)$formattedVal;
				// 	}
				// }
				// throw new DatabaseException(sprintf('Mongo ID was invalid. Got this ID: %s',$formattedVal));
			// }
			return $object;
		}
		
		private function createMongoIDObject(arraykey $id=null)
		{
			try
			{
				return new MongoId($id);
			}
			catch(MongoException $exception)
			{
				return null;
			}
		}

		private function setMongoDateRecursive(DBQuery $object):DBQuery
		{
			$newObject=new Map(null);
			foreach ($object as $key=>$val)
			{
				if ($object[$key] instanceof MongoDate)
				{
					$newObject[$key]=$val;
				}
				else if (stripos($key,self::MONGO_DATE)!==false)
				{
					$newObject[$key]=new MongoDate($val);
				}
				else if (is_array($val))
				{
					$newObject[$key]=$this->setMongoDateRecursiveArray($val);
				}
				else
				{
					$newObject[$key]=$val;
				}
			}
			return $newObject;
		}

		private function setMongoDateRecursiveArray(array<mixed> $object):array<mixed>
		{
			foreach ($object as $key=>$val)
			{
				if ($object[$key] instanceof MongoDate)
				{
					$object[$key]=$val;
				}
				else if (stripos($key,self::MONGO_DATE)!==false)
				{
					$object[$key]=new MongoDate($val);
				}
				else if (is_array($val))
				{
					$object[$key]=$this->setMongoDateRecursiveArray($val);
				}
			}
			return $object;
		}
		
		private function normalizeResults(array<int,array<string,mixed>> $objects):DBVector
		{
			$newResults=new Vector(null);
			for ($i=0,$j=count($objects); $i<$j; $i++)
			{
				$newResults->add($this->normalizeResult($objects[$i]));
			}
			return $newResults;
		}
		
		private function normalizeResult(?array<string,mixed> $object):DBRecord
		{
			$newRecord=new Map(null);

			if(is_null($object))
			{
				return $newRecord;
			}
			foreach ($object as $key=>$val)
			{
				if (is_array($val))
				{
					$newRecord->set($key,$this->arrayToCollection($val));
				}
				//Normalize IDs.
				else if ($key===self::MONGO_ID)
				{
					if ($val instanceof MongoId)
					{
						$val=(string)$val;
					}
					$newRecord->set(self::NORMAL_ID,$val);
				}
				//Ignore normal ids.
				else if ($key!==self::NORMAL_ID)
				{
					$newRecord->set($key,$val);
				}
				//Normalize Date
				else if (strpos(self::MONGO_DATE,$key) !== false)
				{
					if ($val instanceof MongoDate)
					{
						$val=(string)$val;
					}
					$newRecord->set(self::MONGO_DATE,$val);
				}
			}
			return $newRecord;
		}
		
		private function arrayToCollection(array<mixed> $array):Collection
		{
			if (CollectionHelper::isIndexedArray($array))
			{
				$collection=new Vector($array);
			}
			else
			{
				$collection=new Map($array);
			}
			foreach ($collection as $key=>$val)
			{
				if (is_array($val))
				{
					$collection[$key]=$this->arrayToCollection($val);
				}
			}
			return $collection;
		}
		
		/**
		 * Performs a find query (select)
		 * @param  string $collection the collection to run the find query onto
		 * @param  array  $query      a mongo compatible query (an associative array)
		 * @param  array  $fields     a mongo fieldset (associative array of fields to return/inhibit)
		 * @param  array  $sortFields associative array of fields to sort by
		 * @param  integer $limit     maximum number of documents to return
		 * @param  integer $offset     number of documents to skip before collecting results
		 * @return array             the result set of the query
		 */
		// public function findAll(string $collection, array $query =array(), array $fields =array(), ?array $sortFields =null, ?int $limit =null, ?int $offset =null): Map
		// {
		// 	$resultSet	=array();

		// 	try{

		// 		$cursor	=$this->getCollection($collection)->find($query,$fields);
				
		// 		if($sortFields)
		// 		{
		// 			$cursor->sort($sortFields);
		// 		}

		// 		if(is_numeric($limit))
		// 		{
		// 			$cursor->limit($limit);
		// 		}
		// 		if(is_numeric($offset))
		// 		{
		// 			$cursor->skip($offset);
		// 		}

		// 		while($cursor->hasNext())
		// 		{
		// 			$resultSet[]	=$cursor->getNext();
		// 		}
		// 	}
		// 	catch(MongoException $exception)
		// 	{
		// 		throw new DatabaseException($exception->getMessage());
		// 	}
		// 	return new Map($resultSet);
		// }

		/**
		 * Remove documents matching the query filter
		 * @param  string $collection collection name to run the query onto
		 * @param  array   $query       the filter to use for deleting
		 * @param  boolean $justOne    whether to remove only the first matching document
		 *                             defaults to false
		 */
		public function delete(string $collection, DBQuery $query, bool $justOne=false):bool
		{
			// $version=VersionControl::getInstance();
			// if($collection!=self::VERSION_COLLECTION)
			// {
			// 	$version->getBeforeDelete($collection, new Map($query));
			// }
			try 
			{
				$query=$this->parseModelRefsInQuery($query);
				$query=$this->setMongoIdRecursive($query);
				$this->getCollection($collection)->remove
				(
					$query->toArray(),
					[
						'justOne'=>$justOne
					]
				);
				return true;
			}
			catch(MongoException $me)
			{
				throw new DatabaseException($me->getMessage());
			}
			return false;
		}

		/**
		 * Getting Last Error on Mongo DB
		 * @return  String Error Message.
		 */

		public function getLastError():string
		{
			return $this->databasePointer->getLastError();
		}

		/**
		 * Getting Current Using DB Name
		 * @return  String Db name.
		 */

		public function getCurrentDbName():string
		{
			return $this->databasePointer->getName();
		}
		
		public function columnExists(Model $model,string $column):bool
		{
			$query=Map{};
			$result=$this->findOne
			(
				$model->getCollection(),
				Map
				{
					$column=>Map{'$exists'=>true}
				}
			);
			return !is_null($result);
		}

		/**
		 * Create new table inside the database.
		 * @param String $name [Name of Table]
		 * @param array $Options [addtional Option if  need to pass]
		 * @return  Bool.
		 */

		public function createCollection(Model $model, array<mixed,mixed> $options=[]):bool
		{
			try
			{
				// Model::class
				$model->getCollection()
				|> $this->databasePointer->createCollection($$,$options);		
				return true;
			}
			catch(MongoException $exception)
			{
				throw new DatabaseException($exception->getMessage());
			}
		}


		public function getCollectionInfos():array<mixed,mixed>
		{
			return $this->databasePointer->getCollectionInfos();	
		}

		/**
		 * Getting All Tables name inside database.
		 * @return  Array Name of tables.
		 */

		public function getColletionNames():Map<mixed,mixed>
		{
			return new Map($this->databasePointer->getCollectionNames());
		}

		/**
		 * Count Nodes how many records  inside table.
		 * @param String $collection [Name of Table]
		 * @param array $query [addtional parameter if  need to pass]
		 * @return  Bool.
		 */

		public function count(string $collection, array $query):int
		{
			return $this->getCollection($collection)->count($query);
		}
		
		public function collectionExists(string $target):bool
		{
			$collections=$this->listCollections();
			if($collections->linearSearch($target)!==-1)
			{
				return true;
			}
			return false;
		}
		
		public function alterColumn(Model $model,string $column,string $modification):bool
		{
			switch ($modification)
			{
				case 'add':
				{
					//Fields are created automagically by Mongo. So don't bother do do anything here.
					break;
				}
				case 'remove':
				{
					$this->getCollection($model->getCollection())->update
					(
						[],
						['$rename'=>[$column,'__'.$column]],
						false,
						true
					);
					break;
				}
				case 'restore':
				{
					$this->getCollection($model->getCollection())->update
					(
						[],
						['$rename'=>['__'.$column,$column]],
						false,
						true
					);
					break;
				}
			}
			return true;
		}

		/**
		 *  Creates an index in the specified collection on the specified field(s) if it does not already exist. 
		 * @param  [type] $keys [description]
		 * @return [type]       [description]
		 */
		public function createIndex(string $collection, Map<strin ,mixed> $keys):bool
		{
			return $this->getCollection($collection)->createIndex($keys->toArray());
		}

		public function IsWork():bool
		{
			return true;
		}
		
		public function beginTransaction():void
		{
			
		}
		
		public function rollBack():void
		{
			
		}
		
		public function commit():void
		{
			
		}
		
		public function buildColumn(string $name,Map<string,mixed> $annotations):string
		{
			return '';
		}
	}
}
