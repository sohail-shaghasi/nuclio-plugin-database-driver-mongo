Release Notes
-------------
1.1.5
-----
* Added DBOptions to upsert.

1.1.4
-----
* Added missing "use" dependency for DBOptions.

1.1.3
-----
* Now using DBOptions type.

1.1.2
-----
* Added missing interface method Mongo::buildColumn.

1.1.1
-----
* Fixed syntax errors.

1.1.0
-----
* New method Mongo::alterColumn with 3 preset modes: add, remove, restore for use when attempting to safely remove a column without destroying data.
* New method Mongo::columnExists for testing if a column has already been created.

1.0.2
-----
* Fixed PDO path.


1.0.1
-----
* Fixed package name.


1.0.0
-----
* Initial Release.

