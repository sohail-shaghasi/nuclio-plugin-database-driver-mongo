<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\driver\mongo
{
	use nuclio\core\ClassManager;
	use nuclio\plugin\database\common\CommonCursorInterface;
	use nuclio\plugin\database\common\DBRecord;
	use nuclio\plugin\database\common\DBOrder;
	use nuclio\helper\CollectionHelper;
	use \MongoId;
	use \MongoCursor;
	
	<<
		factory,
		provides('database::mongo::cursor')
	>>
	class Cursor implements CommonCursorInterface
	{
		const string MONGO_ID	='_id';
		const string NORMAL_ID	='id';
		private MongoCursor $cursor;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Cursor
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public function __construct(MongoCursor $cursor)
		{
			$this->cursor=$cursor;
		}
		
		public function current():?DBRecord
		{
			$current=$this->cursor->current();
			if (!is_null($current))
			{
				return $this->normalizeResult($current);
			}
			return $current;
		}
		
		public function key():mixed
		{
			return $this->cursor->key();
		}
		
		public function next():void
		{
			$this->cursor->next();
		}
		
		public function rewind():void
		{
			$this->cursor->rewind();
		}
		
		public function valid():bool
		{
			return !is_null($this->current());
		}
		
		public function seek(int $position):void
		{
			$this->cursor->rewind();
			$this->cursor->skip($position);
		}
		
		public function count(bool $foundOnly=false):int
		{
			return $this->cursor->count($foundOnly);
		}
		
		public function limit(int $limit):this
		{
			$this->cursor->limit($limit);
			return $this;
		}
		
		public function offset(int $offset):this
		{
			$this->cursor->skip($offset);
			return $this;
		}
		
		public function orderBy(DBOrder $order):this
		{
			$this->cursor->sort($order->toArray());
			return $this;
		}
		
		private function normalizeResult(array<string,mixed> $object):DBRecord
		{
			$newRecord=new Map(null);
			foreach ($object as $key=>$val)
			{
				if (is_array($val))
				{
					$newRecord->set($key,$this->arrayToCollection($val));
				}
				//Normalize IDs.
				else if ($key===self::MONGO_ID)
				{
					if ($val instanceof MongoId)
					{
						$val=(string)$val;
					}
					$newRecord->set(self::NORMAL_ID,$val);
				}
				//Ignore normal ids.
				else if ($key!==self::NORMAL_ID)
				{
					$newRecord->set($key,$val);
				}
			}
			return $newRecord;
		}
		
		private function arrayToCollection(array<mixed> $array):Collection
		{
			
			if (CollectionHelper::isIndexedArray($array))
			{
				$collection=new Vector($array);
			}
			else
			{
				$collection=new Map($array);
			}
			foreach ($collection as $key=>$val)
			{
				if (is_array($val))
				{
					$collection[$key]=$this->arrayToCollection($val);
				}
			}
			return $collection;
		}
		
		public function __call(string $method, array<mixed> $args):mixed
		{
			return call_user_func_array([$this->cursor,$method],$args);
		}
	}
}
